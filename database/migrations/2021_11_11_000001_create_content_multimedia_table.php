<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentMultimediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_multimedia', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('content_id');
            $table->unsignedBigInteger('multimedia_id');
            $table->unsignedInteger('location')->nullable();
            $table->unsignedInteger('order_column')->nullable();
            $table->char('type', 1);
            $table->unsignedTinyInteger('active')->default(1);
            $table->unsignedTinyInteger('featured')->default(0);

            $table->foreign('multimedia_id')->references('id')->on('multimedia')->cascadeOnDelete();
            $table->foreign('content_id')->references('id')->on('contents')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('content_multimedia');
        Schema::enableForeignKeyConstraints();
    }
}
