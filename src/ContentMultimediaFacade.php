<?php

namespace Bittacora\ContentMultimedia;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\ContentMultimedia\ContentMultimedia
 */
class ContentMultimediaFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'content-multimedia';
    }
}
