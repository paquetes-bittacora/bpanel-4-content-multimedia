<?php

namespace Bittacora\ContentMultimedia\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContentMultimediaLocation extends Model
{
    use HasFactory;

    protected $table = 'content_multimedia_location';
    public $timestamps = false;

    public $fillable = [
        'module', 'name'
    ];
}
