<?php

namespace Bittacora\ContentMultimedia;

use Bittacora\ContentMultimedia\Http\Livewire\DetachMultimediaButton;
use Bittacora\ContentMultimedia\Http\Livewire\ToggleContentMultimediaButton;
use Bittacora\ContentMultimedia\Http\Livewire\UploadContentMultimediaWidget;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\ContentMultimedia\Commands\ContentMultimediaCommand;

class ContentMultimediaServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('content-multimedia')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_content-multimedia_table')
            ->hasCommand(ContentMultimediaCommand::class);
    }

    public function register()
    {
        $this->app->bind('content-multimedia', function($app){
            return new ContentMultimedia();
        });
    }

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        Livewire::component('content-multimedia::upload-content-multimedia-widget', UploadContentMultimediaWidget::class);
        Livewire::component('content-multimedia::detach-multimedia-button', DetachMultimediaButton::class);
        Livewire::component('content-multimedia::toggle-content-multimedia-button', ToggleContentMultimediaButton::class);
        $this->loadViewsFrom(__DIR__ . '/../resources/views/', 'content-multimedia');
    }
}
