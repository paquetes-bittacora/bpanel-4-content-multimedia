<?php

namespace Bittacora\ContentMultimedia;

use Bittacora\ContentMultimediaDocuments\Models\ContentMultimediaDocumentsModel;
use Bittacora\ContentMultimediaImages\ContentMultimediaImages;
use Bittacora\ContentMultimediaImages\ContentMultimediaImagesLocationFacade;
use Bittacora\ContentMultimediaImages\Models\ContentMultimediaImagesModel;
use Bittacora\Multimedia\Models\Multimedia;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use Bittacora\ContentMultimedia\Actions\UploadModelMultimedia;

class ContentMultimedia
{
    public function uploadModelMultimedia(Model $model, UploadedFile|array $files): void
    {
        // No se puede usar inyección de dependencias en las fachadas.
        $uploadModelMultimedia = app()->make(UploadModelMultimedia::class);
        $uploadModelMultimedia->execute($model, $files);
    }

    public function singleUpload(UploadedFile $file)
    {
        $multimedia = Multimedia::create();

        if (in_array($file->getClientOriginalExtension(), Multimedia::$imageExtensions)) {
            $media = $multimedia->addMedia($file)->toMediaCollection('images');
        } elseif (in_array($file->getClientOriginalExtension(), Multimedia::$documentExtensions)) {
            $media = $multimedia->addMedia($file)->toMediaCollection('documents');
        } else {
            $media = $multimedia->addMedia($file)->toMediaCollection('compressed');
        }

        if ($media and $multimedia) {
            return $multimedia;
        }
    }

    public function isMultimediaAssociatedToContent($contentId, $multimediaId, $type)
    {
        if ($type == 'images') {
            $result = ContentMultimediaImagesModel::where('content_id', $contentId)->where('multimedia_id', $multimediaId)->count();
        } elseif ($type == 'documents') {
            $result = ContentMultimediaDocumentsModel::where('content_id', $contentId)->where('multimedia_id', $multimediaId)->count();
        }

        return (bool)$result;
    }

    public function thereIsMultimediaAssociatedToContent($contentId, $type)
    {
        if ($type == 'images') {
            $countContentMultimediaImages = ContentMultimediaImagesModel::where([
                ['content_id', "=", $contentId],
            ])->count();
            return $countContentMultimediaImages;
        } elseif ($type == 'documents') {
            $countContentMultimediaDocuments = ContentMultimediaDocumentsModel::where([
                ['content_id', "=", $contentId],
            ])->count();
            return $countContentMultimediaDocuments;
        }

    }

    public function retrieveContentImages($module, $contentId)
    {
        $moduleImages = [];

        if ($this->thereIsMultimediaAssociatedToContent($contentId, 'images')) {
            $moduleLocations = ContentMultimediaImagesLocationFacade::getModuleLocations($module);
            if (!$moduleLocations->isEmpty()) {
                foreach ($moduleLocations as $location) {
                    $moduleImagesCollection = ContentMultimediaImagesModel::where([
                        ['content_id', '=', $contentId],
                        ['location', '=', $location->id],
                        ['active', '=', 1]
                    ])->orderBy('order_column', 'ASC')->with('multimedia')->get();

                    foreach ($moduleImagesCollection as $moduleImage) {
                        $moduleImages[$location->name][] = $moduleImage->multimedia;
                    }
                }
            } else {

                $moduleImagesCollection = ContentMultimediaImagesModel::where([
                    ['content_id', '=', $contentId],
                    ['active', '=', 1]
                ])->orderBy('order_column', 'ASC')->with('multimedia')->get();


                foreach ($moduleImagesCollection as $moduleImage) {
                    $moduleImages[] = $moduleImage->multimedia;
                }
            }
        }

        return $moduleImages;
    }

    public function retrieveContentDocuments($contentId)
    {
        $moduleDocuments = [];
        if ($this->thereIsMultimediaAssociatedToContent($contentId, 'documents')) {
            $moduleDocuments = ContentMultimediaDocumentsModel::where('content_id', $contentId)->where('active', 1)->orderBy('order_column', 'ASC')->with('multimedia')->get();
        }
        return $moduleDocuments;
    }

    public function retrieveFeaturedImage($contentId)
    {
        $featuredImage = ContentMultimediaImagesModel::where([
            ['content_id', '=', $contentId],
            ['active', '=', 1],
            ['featured', '=', 1]
        ])->with('multimedia')->first();

        if (is_null($featuredImage) or empty($featuredImage)) {
            $featuredImage = ContentMultimediaImagesModel::where([
                ['content_id', '=', $contentId],
                ['active', '=', 1],
            ])->orderBy('order_column', 'ASC')->with('multimedia')->first();
        }

        return $featuredImage;
    }

    public function countContentImages($contentId)
    {
        return ContentMultimediaImagesModel::where([
            ['content_id', '=', $contentId],
            ['active', '=', 1]
        ])->count();
    }

}
