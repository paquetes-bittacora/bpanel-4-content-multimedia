<?php

namespace Bittacora\ContentMultimedia;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\ContentMultimedia\ContentMultimediaLocation
 */
class ContentMultimediaLocationFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ContentMultimediaLocation::class;
    }
}
