<?php

declare(strict_types=1);

namespace Bittacora\ContentMultimedia\Traits;

use Bittacora\ContentMultimedia\ContentMultimediaFacade;
use Bittacora\ContentMultimediaImages\ContentMultimediaImages;
use Bittacora\ContentMultimediaImages\Models\ContentMultimediaImagesModel;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

trait HasImages
{
    /**
     * @return array<Media>
     */
    public function getImages(): array
    {
        $output = [];
        $media = ContentMultimediaFacade::retrieveContentImages('products', $this->content->id);

        foreach ($media as $image) {
            $output[] = $image->getMedia('images')->first();
        }

        return $output;
    }

    public function getFeaturedImages(?string $location = null): array
    {
        $output = [];

        $contentId = $this->content->id;
        $media = ContentMultimediaFacade::retrieveContentImages('products', $contentId);

        foreach ($media as $image) {
            $contentMultimediaImage = ContentMultimediaImagesModel::where('multimedia_id', $image->id)
            ->where('content_id', $contentId)->first();

            if (null !== $contentMultimediaImage && $contentMultimediaImage->featured == 1) {
                $output[] = $image->getMedia('images')->first();
            }
        }

        return 0 === count($output) ? $this->getImages() : $output;
    }
}
