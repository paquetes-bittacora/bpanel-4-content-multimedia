<?php

declare(strict_types=1);

namespace Bittacora\ContentMultimedia\Actions;

use Bittacora\ContentMultimedia\ContentMultimediaFacade;
use Bittacora\ContentMultimediaDocuments\Models\ContentMultimediaDocumentsModel;
use Bittacora\ContentMultimediaImages\Models\ContentMultimediaImagesModel;
use Bittacora\Multimedia\MultimediaFacade;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

/**
 * Acción para asociar multimedia a un modelo
 */
class UploadModelMultimedia
{
    /**
     * @param Model $model El modelo al que queremos asociar los archivos
     * @param UploadedFile|array $files El archivo o array que nos devuelve el método $request->validated() para el
     *                                  campo tipo 'file' del que queremos leer los archivos
     * @return void
     */
    public function execute(Model $model, UploadedFile|array $files): void
    {
        if (!is_array($files)) {
            $files = [$files];
        }

        $filesCount = count($files);

        for ($i = 0; $i < $filesCount; $i++) {
            $this->uploadFile($files[$i], $model);
        }
    }

    private function uploadFile(UploadedFile $file, Model $model): void
    {
        $file = ContentMultimediaFacade::singleUpload($file);

        if (!$file) {
            return;
        }

        $fileExtension = $file->mediaModel()->first()->extension;

        if (in_array($fileExtension, MultimediaFacade::getImageExtensions())) {
            $contentMultimediaModel = new ContentMultimediaImagesModel();
        } elseif (in_array($fileExtension, MultimediaFacade::getDocumentExtensions())) {
            $contentMultimediaModel = new ContentMultimediaDocumentsModel();
        }

        $contentMultimediaModel->fill(['content_id' => $model->content->id, 'multimedia_id' => $file->id])->save();
    }
}
