<?php

namespace Bittacora\ContentMultimedia\Http\Livewire;

use Bittacora\ContentMultimedia\ContentMultimediaFacade;
use Bittacora\ContentMultimediaDocuments\Models\ContentMultimediaDocumentsModel;
use Bittacora\ContentMultimediaImages\Models\ContentMultimediaImagesModel;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use function view;

class ToggleContentMultimediaButton extends Component
{

    use LivewireAlert;

    public bool $attached;
    public int $contentId;
    public int $multimediaId;
    public string $permission;
    public string $type;

    protected $listeners = ['refreshToggleButton' => 'refreshToggleButton'];

    protected function getListeners()
    {
        return [
            "refreshToggleButton$this->multimediaId" => 'refreshToggleButton'
        ];
    }

    public function mount($type){
        $this->attached = ContentMultimediaFacade::isMultimediaAssociatedToContent($this->contentId, $this->multimediaId, $type);
    }

    public function render(){
        return view('content-multimedia::livewire.toggle-content-multimedia-button');
    }

    public function toggleAttachment(){
        $result = false;
        if(!$this->attached){
            if($this->type == "images"){
                $result = ContentMultimediaImagesModel::create([
                    'content_id' => $this->contentId,
                    'multimedia_id' => $this->multimediaId,
                    'active' => 1
                ]);
            }elseif($this->type == "documents"){
                $result = ContentMultimediaDocumentsModel::create([
                    'content_id' => $this->contentId,
                    'multimedia_id' => $this->multimediaId,
                    'active' => 1
                ]);
            }

        }else{
            if($this->type == "images"){
                $result = ContentMultimediaImagesModel::where('content_id', $this->contentId)->where('multimedia_id', $this->multimediaId)->delete();
            }elseif($this->type == "documents"){
                $result = ContentMultimediaDocumentsModel::where('content_id', $this->contentId)->where('multimedia_id', $this->multimediaId)->delete();
            }
        }

        if($result){
            $this->attached = !$this->attached;
            if($this->attached){
                $this->alert('success', 'Archivo adjuntado con éxito');
            }else{
                $this->alert('success', 'Archivo desenlazado con éxito');
            }
            if($this->type == "images"){
                $items = ContentMultimediaImagesModel::where('content_id', $this->contentId)->orderBy('order_column', 'ASC')->pluck('id');
                ContentMultimediaImagesModel::setNewOrder($items);
                $this->emit('refreshContentMultimediaImagesWidgetTable');
            }elseif($this->type == "documents"){
                $items = ContentMultimediaDocumentsModel::where('content_id', $this->contentId)->orderBy('order_column', 'ASC')->pluck('id');
                ContentMultimediaDocumentsModel::setNewOrder($items);
                $this->emit('refreshContentMultimediaDocumentsWidgetTable');
            }
        }else{
            $this->alert('error', 'Error al adjuntar el archivo al contenido.');
        }
    }

    public function refreshToggleButton(){
        $this->attached = !$this->attached;
    }
}
