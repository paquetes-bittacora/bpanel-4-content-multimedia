<?php

namespace Bittacora\ContentMultimedia\Http\Livewire;

use Bittacora\ContentMultimedia\Models\ContentMultimedia;
use Bittacora\Multimedia\Models\Multimedia;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class ContentMultimediaImagesWidget extends Component
{

    public int $contentId;
    public ?Collection $images = null;

    public function mount()
    {
        $contentMultimediaData = ContentMultimedia::where([
            ['content_id', '=', $this->contentId],
            ['type', '=', 'I']
        ])->orderBy('order_column', 'ASC')->get();

        if (!empty($contentMultimediaData)){
            foreach ($contentMultimediaData as $key => $content) {
                /**
                 * @var ContentMultimedia $content
                 */
                $multimedia = Multimedia::where('id', $content->multimedia_id)->with('mediaModel')->first();
//                $content->setAttribute('thumbnail', $multimedia->mediaModel()->first());
                $content->setAttribute('multimedia', $multimedia);
            }

            $this->images = $contentMultimediaData;
        }
    }

    public function render()
    {
        return view('content-multimedia::livewire.content-multimedia-images-widget')->with([
            'images' => $this->images,
            'contentId' => $this->contentId
        ]);
    }
}
