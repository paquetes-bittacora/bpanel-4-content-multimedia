<?php

namespace Bittacora\ContentMultimedia\Http\Livewire;

use Illuminate\Database\Eloquent\Model;
use Livewire\Component;

class UploadContentMultimediaWidget extends Component
{
    public string $fieldName = 'file';
    public ?Model $model = null;
    public ?string $allowedFormats = null;
    public ?string $allowedExtensions = null;
    public bool $multiple = true;

    public function render()
    {
        return view('content-multimedia::livewire.upload-content-multimedia-widget')->with([
            'model' => $this->model,
            'allowedFormats' => $this->allowedFormats,
            'allowedExtensions' => $this->allowedExtensions
        ]);
    }
}
