<?php

namespace Bittacora\ContentMultimedia\Http\Livewire;

use Bittacora\ContentMultimediaDocuments\Models\ContentMultimediaDocumentsModel;
use Bittacora\ContentMultimediaImages\Models\ContentMultimediaImagesModel;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class DetachMultimediaButton extends Component
{
    use LivewireAlert;
    public string $permission;
    public int $contentMultimediaId;
    public int $contentId;
    public string $type;

    protected $listeners = ['detachFile', 'deleteConfirmation'];

    public function render()
    {
        return view('content-multimedia::livewire.detach-multimedia-button');
    }

    public function getListeners()
    {
        return [
            'deleteConfirmation' => 'deleteConfirmation',
            'detachFile'.$this->type.$this->contentMultimediaId => 'detachFile'
        ];
    }

    public function deleteConfirmation($id){
        $this->dispatchBrowserEvent("swal", [
            'type' => 'warning',
            'title' => '¿Estás seguro?',
            'text' => 'Esta acción no se podrá deshacer',
            'id' => $id,
            'filetype' => $this->type
        ]);
    }

    public function detachFile($id){
        if($this->type == "I"){
            $multimediaId = ContentMultimediaImagesModel::where('id', $id) -> pluck('multimedia_id')->first();
            $result = ContentMultimediaImagesModel::where('id', $id)->delete();
        }elseif($this->type == "D"){
            $multimediaId = ContentMultimediaDocumentsModel::where('id', $id) -> pluck('multimedia_id')->first();
            $result = ContentMultimediaDocumentsModel::where('id', $id)->delete();
        }

        if($result){
            if($this->type == "I"){
                $items = ContentMultimediaImagesModel::where('content_id', $this->contentId)->orderBy('order_column', 'ASC')->pluck('id');
                ContentMultimediaImagesModel::setNewOrder($items);
                $this->emit('refreshContentMultimediaImagesWidgetTable');
            }elseif($this->type == "D"){
                $items = ContentMultimediaDocumentsModel::where('content_id', $this->contentId)->orderBy('order_column', 'ASC')->pluck('id');
                ContentMultimediaDocumentsModel::setNewOrder($items);
                $this->emit('refreshContentMultimediaDocumentsWidgetTable');

            }

            $this->emit("refreshToggleButton$multimediaId");
            $this->alert('success', 'La imagen ha sido desenlazada del contenido.');
        }else{
            $this->alert('error', 'Error al realizar la acción.');
        }

    }
}
