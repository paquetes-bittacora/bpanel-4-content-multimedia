<?php

namespace Bittacora\ContentMultimedia\Http\Livewire;

use Bittacora\ContentMultimedia\ContentMultimediaLocationFacade;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Livewire\Component;

class ContentMultimediaLocationSelect extends Component
{
    public string $module;
    public Collection $options;
    public Model $model;
    public $selectedValue = null;

    public function mount(){
        $this->options = ContentMultimediaLocationFacade::getModuleLocations($this->module);
        $this->selectedValue = $this->model->location;
    }
    public function render()
    {
        return view('content-multimedia::livewire.content-multimedia-location-select')->with([
            'options' => $this->options,
            'model' => $this->model
        ]);
    }

    public function changeLocation(){
        if($this->selectedValue != ''){
            $result = $this->model->update(['location' => $this->selectedValue]);
        }else{
            $result = $this->model->update(['location' => null]);
        }

        if(!$result){
            session()->flash('message', ['text' => 'Error al actualizar.', 'type' => 'danger', 'icon' => 'fa fa-times-circle']);
        }else{
            session()->flash('message', ['text' => 'Posición actualizada.', 'type' => 'success', 'icon' => 'fa fa-check-circle']);
        }
    }
}
