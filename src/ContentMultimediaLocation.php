<?php

namespace Bittacora\ContentMultimedia;

class ContentMultimediaLocation
{
    public function createNewLocation(string $module, string $name){
        Models\ContentMultimediaLocation::create([
            'module' => $module,
            'name' => $name
        ]);
    }

    public function getModuleLocations(string $module){
        return Models\ContentMultimediaLocation::where('module', $module)->get();
    }
}
