<?php

namespace Bittacora\ContentMultimedia\Commands;

use Illuminate\Console\Command;

class ContentMultimediaCommand extends Command
{
    public $signature = 'content-multimedia';

    public $description = 'My command';

    public function handle()
    {
        $this->comment('All done');
    }
}
