<?php

use Bittacora\ContentMultimedia\Http\Controllers\ContentMultimediaController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel')->middleware(['web', 'auth','admin-menu'])->name('content-multimedia.')->group(function(){
    Route::post('content-multimedia/reorder', [ContentMultimediaController::class, 'reorder'])->name('reorder');
    Route::post('content-multimedia/detach', [ContentMultimediaController::class, 'detach'])->name('detach');
});
