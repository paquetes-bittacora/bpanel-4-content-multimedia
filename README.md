# Content multimedia

Paquete para facilitar asociar multimedia a contenidos. El modelo al que se
quiera asociar multimedia debe tener además un id como contenido (ver paquete
[bpanel4-content](https://gitlab.com/paquetes-bittacora/bpanel-4-contenidos)).

## Instalación

Hay que añadir las siguientes entradas al array `disks` de `config/filesystems.php` de Laravel.

```php
'images' => [
    'driver' => 'local',
    'root' => storage_path('app/public/media/images'),
    'url' => env('APP_URL') . '/storage/media/images'
],
'documents' => [
    'driver' => 'local',
    'root' => storage_path('app/public/media/documents'),
    'url' => env('APP_URL') . '/storage/media/documents'
],
'compressed' => [
    'driver' => 'local',
    'root' => storage_path('app/public/media/compressed'),
    'url' => env('APP_URL') . '/storage/media/compressed'
]
```


## Como guardar multimedia en un modelo:

En el ejemplo, `$page` es un modelo que además está asociado al módulo de 
contenidos. En el método `update` y `store` del controlador, debe añadirse
lo siguiente:

```php 
ContentMultimedia::uploadModelMultimedia($page, $file);
```

`$file` es el array o archivo individual devuelto por `$request->validated()`
para el campo 'file' correspondiente. Es decir, es el archivo o archivos a
subir pero no leido desde `$_FILES` sino desde el `Request` de Laravel.

**Campo para subir archivos en un template**

En la vista desde la que se llame al controlador, deberá incluirse el componente
de livewire `upload-content-multimedia-widget`:

```php
@livewire('content-multimedia::upload-content-multimedia-widget', [
    'fieldName' => 'file',
    'model' => $page,
    'allowedFormats' => 'image/jpeg, image/png',
    'allowedExtensions' => 'jpg,jpeg,png'
])
```

## Mostrar multimedia asociado a un modelo:
También puede incluirse este otro componente, que mostrará las imágenes, archivos, etc
asociados al modelo:

```php 
@livewire('content-multimedia-images::content-multimedia-images-widget', [
    'contentId' => $page,
    'module' => 'page',
    'permission' => 'page.edit'
])
```

# Traits

El trait `HasImages` añade el método `getImages()` al modelo, facilitando obtener
las imágenes que tiene asociadas como colección de objetos `Media` de **Spatie media library**.
