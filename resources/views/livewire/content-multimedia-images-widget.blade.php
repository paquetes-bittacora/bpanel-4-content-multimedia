<div class="widget-box widget-color-blue ui-sortable-handle mb-4" id="widget-box-7">
    <div class="widget-header widget-header-small">
        <h6 class="widget-title smaller">Imágenes adjuntas</h6>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            @if(!is_null($images))
                <table data-content-id="{{$contentId}}" id="contentMultimediaImagesWidgetTable" class="mb-0 table table-borderless table-bordered-x brc-secondary-l3 text-dark-m2 radius-1 overflow-hidden">
                    <thead>
                    <tr>
                        <th class="text-center w-10">Imagen</th>
                        <th class="w-30">Título</th>
                        <th class="text-center w-20">Posición</th>
                        <th class="text-center w-10">Orden</th>
                        <th class="text-center w-10">Activo</th>
                        <th class="text-center w-10">Destacado</th>
                        <th class="text-center w-10"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($images as $image)
                        <tr id="{{$image->id}}" data-content-id="{{$image->content_id}}">
                            <td>
                                @livewire('utils::media-preview', ['media' => $image->multimedia->mediaModel()->first()])
                            </td>
                            <td>
                                @livewire('utils::model-translatable-attribute', ['model' => $image->multimedia, 'attribute' => 'title'])
                            </td>
                            <td>
                                @livewire('content-multimedia::content-multimedia-location-select', ['model' => $image, 'module' => 'page'])
                            </td>
                            <td class="dragHandle cursor">
                                <div class="d-flex justify-content-center align-items-center" style="pointer-events: none">
                                    <span class="align-items-center badge bgc-purple-d1 pos-rel text-white radius-4 px-3">
                                        <span class="bgc-primary-tp4 opacity-5 position-tl h-100 w-100 radius-4"></span>
                                        <span class="pos-rel">
                                            {{$image->order_column}}
                                        </span>
                                    </span>
                                </div>
                            </td>
                            <td class="text-center">
                                @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $image, 'value' => $image->active, 'size' => 'xxs'])
                            </td>
                            <td class="text-center">
                                @livewire('utils::datatable-default', ['fieldName' => 'featured', 'model' => $image, 'value' => $image->featured, 'size' => 'xxs'])
                            </td>
                            <td>
                                <div class="buttons d-flex">
                                    <a class="btn btn-xs btn-outline-danger button-detach" onclick="return confirm('¿Quieres desenlazar este recurso del contenido?')">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-warning">
                    No hay imágenes adjuntas al contenido.
                </div>
            @endif
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function() {
            // Initialise the table
            $("#contentMultimediaImagesWidgetTable").tableDnD({
                'onDrop': function(table, row){
                    console.log(row.rowIndex);
                    var rows = [];
                    console.log($(table).find('tr'));

                    $.each($(table).find("tbody").find("tr"), function(key, value){
                        rows[key+1] = $(value).attr('id');
                    });

                    $.ajax({
                        method: 'post',
                        url: "{{route("content-multimedia.reorder")}}",
                        data: {
                            'contentId':{{$contentId}},
                            'type' : 'I',
                            'rows' : rows
                        },
                        context: document.body
                    }).done(function(data) {
                        data = JSON.parse(data);

                        $.each(data, function(key, value){
                            $("tr#"+key).find(".pos-rel").text(value);
                        });
                    });
                },
                'onDragClass' : 'bg-info',
                'dragHandle' : '.dragHandle',
            });

            $("body").on("click", ".button-detach", function(){
                var id = $(this).parents('tr').attr('id');
                var row = $(this).parents('tr')[0].rowIndex;
                $.ajax({
                    method: 'post',
                    url: "{{route("content-multimedia.detach")}}",
                    data: {
                        'id':id,
                        'type':'I',
                        'contentId':$(this).parents('tr').data('content-id')
                    },
                    context: document.body
                }).done(function(data) {

                    if(data != 'error'){
                        $("tr#"+id).addClass("bg-danger");
                        setTimeout(function(){
                            $("tr#"+id).fadeOut(1000);
                            document.getElementById("contentMultimediaImagesWidgetTable").deleteRow(row);
                        },500);

                        data = JSON.parse(data);

                        $.each(data, function(key, value){
                            $("tr#"+key).find(".pos-rel").text(value);
                        });
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'El elemento no pudo ser eliminado del contenido',
                            showConfirmButton: false,
                            timer: 2000
                        });
                    }
                });
            });
        });
    </script>

@endpush
