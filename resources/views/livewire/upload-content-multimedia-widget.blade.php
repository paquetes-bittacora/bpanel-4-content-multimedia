<div class="widget-box widget-color-blue ui-sortable-handle mb-4" id="widget-box-7">
    <div class="widget-header widget-header-small">
        <h6 class="widget-title smaller">Añadir multimedia</h6>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            @livewire('form::input-file', [
                'name' => $fieldName . '[]',
                'idField' => $fieldName,
                'fieldWidth' => 12,
                'required' => false,
                'accept' => $allowedFormats,
                'multiple' => $multiple,
                'allowedFileExtensions' => $allowedExtensions,
                'showUpload' => false,
                'showRemove' => true
            ])
            @if($errors->has($fieldName))
                <ul>
                    @foreach($errors->get($fieldName . '.*') as $errors)
                        @foreach($errors as $error)
                            <li class="text-danger">{{ $error }}</li>
                        @endforeach
                    @endforeach
                </ul>
            @endif
            @if($errors->has($fieldName . '.*'))
                <ul>
                    @foreach($errors->get($fieldName . '.*') as $errors)
                        @foreach($errors as $error)
                            <li class="text-danger">{{ $error }}</li>
                        @endforeach
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
</div>
