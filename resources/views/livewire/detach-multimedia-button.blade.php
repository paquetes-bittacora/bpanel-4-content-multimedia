<div>
    <div class="buttons d-flex">
        <a class="btn btn-xs btn-outline-danger button-detach" wire:click.prevent="deleteConfirmation({{$contentMultimediaId}}, '{{$type}}')">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>

@push('scripts')
    <script>
        window.addEventListener('swal', event => {
            Swal.fire({
                icon: event.detail.type,
                title: event.detail.title,
                text: event.detail.text,
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sí',
                cancelButtonText: 'Cancelar',
                confirmButtonColor: 'success'
            }).then((result) => {
                if(result.isConfirmed){
                    Livewire.emit('detachFile'+event.detail.filetype+event.detail.id, event.detail.id);
                }
            });
        });
    </script>
@endpush
