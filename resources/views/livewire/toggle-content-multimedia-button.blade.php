<div class="text-center">
    @if ($attached)
        <a class="btn btn-danger btn-xs mb-1"
           wire:click="toggleAttachment()"
           wire:loading.class="btn-warning"
           title="Desactivado">
            <i class="fa fa-minus" wire:loading.remove></i>
            <div wire:loading>
                <i class="fa fa-cog fa-spin fa-fw"></i>
            </div>
        </a>
    @else
        <a class="btn btn-success btn-xs mb-1"
           wire:click="toggleAttachment()"
           wire:loading.class="btn-warning"
           title="Activado">
            <i class="fa fa-plus" wire:loading.remove></i>
            <div wire:loading>
                <i class="fa fa-cog fa-spin fa-fw"></i>
            </div>
        </a>
    @endif
</div>
